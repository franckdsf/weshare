<?php
  class Router{
    static $baseURL;
    static $base_url_files = __DIR__.'/../routes/';
    
    //add a base URL for requests
    public static function baseURL($url){
      self::$baseURL = $url;
    }

    //Run function if URI is good
    // example with /api/product/surface-book-2/3?source=instagram
    static function runFunctionIfGoodURI($url, $function){
      //remove query params from URL : url => /api/product/surface-book-2/3
      $finalURL = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);
      $baseURL = self::$baseURL.$url;
      $params = self::returnsParams($url, $finalURL);

      //construct url again from baseURL + params
      // url : null
      if(strpos($baseURL, '/:') !== false){
        $finalURL = substr($finalURL, 0, strpos($baseURL, '/:'));
        // url : /api/product
        foreach ($params as $key => $value) {
          $finalURL .= '/:'.$key;
        }
        // loop 1 - url : /api/product/:id
        // loop 2 - url : /api/product/:id/:variant
      }

      // compare templateURL & generated URL
      if($finalURL == $baseURL){
        $route = (object)[];
        $route->params = (object)$params;
        $route->path = $_SERVER['REQUEST_URI'];
        $route->query = (object)$_GET;
        $route->body = (object)$_POST;
        $route->files = (object)$_FILES;
        //call the function passed in parameters of runFunctionIfGoodURI() 
        $function($route);
        die;
      }
    }

    // get params keys from baseURL
    //example with url : /api/product/:id/:variant
    static function getBaseParams($url){
      $params = [];
      // url : /product/:id/:variant
      $modifiedURL = str_replace(self::$baseURL, '', $url);
      $pos = strpos($modifiedURL, '/:');
      while($pos !== false){
        // url : id/:variant
        $modifiedURL = substr($modifiedURL, $pos+2);
        if(strpos($modifiedURL, '/'))
          // value = id
          $value = substr($modifiedURL, 0, strpos($modifiedURL, '/'));
        else
          $value = substr($modifiedURL, 0);
        //add id to array
        $params[] = $value;
        $pos = strpos($modifiedURL, '/:');
        //loop again with /:variant
      }

      // return Array( [0] => id, [1] => variant )
      return $params;
    }

    // get params keys & values from url
    //example with url : /api/product/surface-book-2/3
    static function returnsParams($baseURL, $url){
      // get keys from baseURL : /api/product/:id/:variant | Array ( [0] => id, [1] => variant )
      $baseParams = self::getBaseParams($baseURL);
      $values = [];
      $finalParams = [];

      // get position of first params in /api/product/:id/:variant | 11
      $pos = strpos(self::$baseURL.$baseURL, '/:');
      if($pos !== false){
        // truncate URL : /api/product/surface-book-2/3 => /surface-book-2/3
        $url = substr($url, $pos);
      }

      $pos = strpos($url, '/');
      while($pos !== false){
        // truncate URL starting with first / : /surface-book-2/3 => surface-book-2/3
        $url = substr($url, $pos+1);
        if(strpos($url, '/'))
          // truncate URL starting from 0 to position of /, and set value | $value = surface-book-2
          $value = substr($url, 0, strpos($url, '/'));
        else
          $value = substr($url, 0);
        // add Array ( [0] => surface-book-2 )
        $values[] = $value;
        $pos = strpos($url, '/');
        // start again with surface-book-2/3
      }

      // $values = Array ( [0] => surface-book-3, [1] => 1 )
      // $baseParams = Array ( [0] => id, [1] => variant )
      for ($i=0; $i < sizeof($baseParams); $i++) { 
        if(isset($values[$i]))
          $finalParams[$baseParams[$i]] = $values[$i];
      }

      // $finalParams = Array ( [id] => surface-book-3, [variant] => 1 )
      return $finalParams;
    }

    // dynamically generate post, get, delete, put, ect.. functions
    // check if request method is equal to the name of the function called
    // if yes, check if URL is euqal to the one passed in params : params[0]
    public static function __callStatic($function, $params){
      if($_SERVER['REQUEST_METHOD'] != strtoupper($function))
        return;

      self::runFunctionIfGoodURI($params[0], $params[1]);
    }
  }
?>