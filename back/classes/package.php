<?php
  class Package {
    private $id;
    private $path;
    private $name;
    private $files;
    private $message;

    public function __construct(array $donnees){
        if (!empty($donnees)){
            $this->hydrate($donnees);
        }    
    }

    //automatically hydrate class from an array
    public function hydrate(array $donnees){
      foreach ($donnees as $key => $value){
        $method = 'set'.ucfirst($key);
                  
        if (method_exists($this, $method)){
          $this->$method($value);
        }
      }
    } 

    public function toObject(){
        $data = (object)[];
        $data->id = $this->id;
        $data->name = $this->name;
        $data->path = $this->path;
        $data->files = [];
        foreach ($this->files as $file) {
            $data->files[] = $file->toObject();
        }
        $data->message = $this->message;
        return $data;
    }  

    /**
     * Get the value of files
     */ 
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set the value of files
     *
     * @return  self
     */ 
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of path
     */ 
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     *
     * @return  self
     */ 
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
  }

?>