<?php
require_once __DIR__.'/../database.php';
require_once __DIR__.'/../../classes/file.php';

class FilesDB{  

  // converts a package from DB into an object of package class
  private static function fileDBtoFile($fileDB){
    $additional = pathinfo($fileDB['name']);

    $data = (object)[];
    $data->name = $fileDB['name'];
    $data->path = $fileDB['package'];
    $data->filename = $additional['filename'];
    $data->extension = $additional['extension'];

    $file = new File((Array)$data);
    
    return $file;
  }
  
  public static function findByPackage($packageId){
    Database::instance()->beginTransaction();
    try{
      $query = Database::instance()->prepare('SELECT * FROM files where package = :package');
      $query->bindValue(':package', $packageId);
      $query->execute();
    
      Database::instance()->commit();

      $result = [];
      foreach($query->fetchAll() as $file){
        $result[] = self::fileDBtoFile($file);
      }
      return $result;
    }
    catch(Exception $error){
      Database::instance()->rollback();
      Utils::error(500, 'internal error while trying to find files by package id : '.$error->getMessage());
    }
  }

  public static function save(Array $files){
    if(sizeof($files) == 0)
      Utils::error(500, 'internal error while saving files to database : empty array of files');

    Database::instance()->beginTransaction();

    foreach($files as $file){
      try{
        $query = Database::instance()->prepare("INSERT INTO files (name, package) VALUES (:name, :package)");
        $query->bindValue(':name', $file->getName());
        $query->bindValue(':package', $file->getPath());
        $query->execute();
      }
      catch(Exception $error){
        Database::instance()->rollback();
        Utils::error(500, 'filesDB : internal error while saving files to database : '.$error->getMessage());
      }
    }

    Database::instance()->commit();

    return self::uploadFiles($files[0]->getPath(), $files);
  }
  
  private static function uploadFiles($id, Array $files){
    //create directory to store files
    mkdir(__DIR__."/../storage/files/$id");

    // create zip Archive
    $zip = new ZipArchive;
    $zip->open(__DIR__."/../storage/files/$id/".'we_share_'.$id.'.zip', ZipArchive::CREATE);

    foreach($files as $file){
      $newname = $file->getFileName().".".$file->getExtension(); 

      // [REMOVED] upload files to folder
      // $destination = __DIR__."/../storage/files/$id/$newname";
      // move_uploaded_file($file->getTmpName(), $destination);

      // add each file to zip archive
      $zip->addFromString($newname, file_get_contents($file->getTmpName()));
    }
    $zip->close();

    return true;
  }
}

?>