<?php
  require_once __DIR__.'/scripts/filesAPI.php';
  require_once __DIR__.'/scripts/packageAPI.php';

  class Api{
    private static $files;
    private static $packages;

    public static function init(){
      self::$files = new FilesAPI();
      self::$packages = new PackagesAPI();
    }

    public static function files(){
      return self::$files;
    }
    public static function packages(){
      return self::$packages;
    }
  }

  Api::init();
?>