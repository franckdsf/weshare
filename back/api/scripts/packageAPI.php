<?php
  require_once __DIR__.'/../api.php';
  require_once __DIR__.'/../../database/database.php';
  require_once __DIR__.'/../../classes/package.php';
  require_once __DIR__.'/../../classes/file.php';
  
  class PackagesAPI{
    public static function save($restFiles, $restMessage){
      $donnees = (object)[];
      $donnees->files = $restFiles;
      $donnees->message = $restMessage;
      //create a package with data
      $package = new Package((Array)$donnees);

      //save package on database
      $package = Database::packages()::saveOne($package);

      //upload each files on the server
      $files = Api::files()::upload($package, $restFiles);
      $package->setFiles($files);
      $package->setPath($package->getId());

      //return the package to client
      Utils::success(201, $package->toObject());
    }

    public static function findById($restId){
      $package = Database::packages()::findById($restId);

      //if no package found
      if(!$package)
        Utils::error(500, "can't find package with id : $restId");
      else{
        //get every files from package
        $files = Database::files()::findByPackage($package->getId());
        $package->setFiles($files);

        //return the package to client
        Utils::success(200, $package->toObject());
      }
    }
  }

?>