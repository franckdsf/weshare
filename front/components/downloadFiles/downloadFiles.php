<div class="download_files">
  <div id="download_icon">
    <i class="fas fa-arrow-circle-down"></i>
    <i class="fas fa-exclamation-circle"></i>
  </div>
  <div id="files_cont">
  </div>
  <div class="message_cont">
    <div class="message"></div>
  </div>
  <div id="button_link" class="button_link inactive">
    Download files
  </div>
</div>

<link rel="stylesheet" href="front/components/downloadFiles/downloadFiles.css" type ="text/css">
<script type="module" src="front/components/downloadFiles/downloadFiles.js"></script>
