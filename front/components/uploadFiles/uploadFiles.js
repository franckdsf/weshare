import {$} from '../../assets/js/jquery_like.js';

class UploadFilesComponent{
  files = [];
  baseURL = window.origin+'/api';
  zipId = null;

  constructor(){
    const self = this;
    //on click on fake upload files button, activate real upload files button
    $(".upload_files #add_files").addEventListener("click", () => {$('.upload_files #files_importer').click()});
    //on files imported changing, refresh view
    $(".upload_files #files_importer").addEventListener("change", function(e){
      self.files_importer(e.target.files,self);
    });
    //on get link button clicked, upload files & retrieve the URL to share
    $(".upload_files #button_link").addEventListener("click", function(e){
      self.get_link_buttton(self);
    });
    //on copy URL button clicked, copy URL to share
    $(".upload_files #copy_button_link").addEventListener("click", function(e){
      self.set_link_button(e.target, self.zipId);
    });

    //init component
    this.reset();
  }

  files_importer = (fileList, self) => {
    $(".upload_files #files_cont").innerHTML = "";
    self.files = Array.from(fileList);
    let totalSize = 0;
    const max_number_file = 500;
    const max_size_total_files = 20000000;
    const max_size_single_file = max_size_total_files;

    //foreach files, check size of the package
    //if it's over 29MB, alert user & reset
    //if one of the file size is over 19MB, alert user & reset
    //if user uploads more than the maximum number of files allowed, alert user & reset
    let i = 0;
    self.files.forEach(element => {     
      //if element size is 0 / if it's a directory
      if(element.size != 0){
        totalSize += element.size;
        //create div to show file information on the view
        self.previewFile(element);

        //if it's over 20MB, alert user & reset
        //if the file size is over 20MB, alert user & reset
        if(element.size > max_size_single_file || totalSize > max_size_total_files){
          alert("Can't upload the files. A file is too big (over 20Mb) or The total files size is over 20Mb.");
          self.reset();
          return;
        };

        //if user uploads more than the maximum number of files allowed, alert user & reset
        if(i >= max_number_file-1){
          alert("Please upload less than "+max_number_file+" files.");
          self.reset();
          return;
        }
      }
      i++;
    });

    if(i > 0){
      // activate button to generate the link
      $(".upload_files #button_link").classList.remove("inactive");
      $(".upload_files #add_files").style.display = "none";
    }
  }

  //reset component & view
  reset = () => {
    const self = this;
    $(".upload_files #files_cont").innerHTML = "";
    $(".upload_files #files_importer").value = '';
    $(".upload_files #button_link").classList.add("inactive");
    $(".upload_files #button_link").innerHTML = "Generate the link";
    $(".upload_files #copy_button_link").classList.add("inactive");
    $(".upload_files #copy_button_link").style.display = "none";
    $(".upload_files #button_link").style.display = "block";
    $(".upload_files #add_files").style.display = "flex";
    self.files = [];
    self.zipId = null;
  }

  //show element on view
  previewFile = (file) => {
    const self = this;
    let size = Math.floor(file.size / 1000) + 1;
    let div = document.createElement("div");
      div.innerHTML = "<div class='name'>"+file.name+"</div>";
      div.innerHTML += "<div class='size'>"+size+" Ko</div>";
      div.innerHTML += "<div class='delete' id="+file.lastModified+">X</div>";
  
    $(".upload_files #files_cont").append(div);
    //add eventlistener on the delete button
    document.getElementById(file.lastModified).addEventListener("click",function(e){
      let index = -1;
      let i = 0;
  
      //on click on a delete button
      //check the button id, if it equals a file.lastModified variable
      self.files.forEach(element => {
        if(element.lastModified == file.lastModified)
          index = i;
        i++;
      });
  
      //delete it from the array
      if(index != -1)
        self.files.splice(index,1);
      //if no more files are in the array
      //reset the view
      if(self.files.length == 0){
        self.reset();
      }
      //remove the div from screen
      div.remove();
    });
  }

  get_link_buttton = (self) => {
    //dont upload files if no files are in the array
    if(self.files.length <= 0 || self.zipId != null)
      return;

    //create dataSet to send to the server
    //get message from the textArea of the view
    //set all files in a formData 
    //add message in the formData
    let message = $(".upload_files #message").value;
    const formData = new FormData();
    let i = 0;
    self.files.forEach(file => {
      formData.append('file_'+i, file);
      i++;
    });
    formData.append('message', message);

    //disable textarea so user can't modify its message
    //disable delete files so user can't delete them
    //disable get link button & modify its innerHTML
    $(".upload_files #button_link").classList.add("inactive");
    $(".upload_files #button_link").innerHTML = 'Upload in progress <i class="fas fa-circle-notch fa-spin" style="margin-left:0.3em"></i>';
    $(".upload_files #message").setAttribute("disabled",true);
    document.querySelectorAll(".upload_files .delete").forEach(element => {
      element.remove();
    });

    //send files to API
    fetch(self.baseURL+'/files',{
      method: 'POST',
      body: formData
    }).then(async result => {
      if(result.ok)
        return result.json();
      throw await result.json();
    })
    .then(async response => {
      //change button to "copy URL to share" button
      $(".upload_files #copy_button_link").classList.remove("inactive");
      $(".upload_files #copy_button_link").style.display = "block";
      $(".upload_files #button_link").style.display = "none";
  
      self.zipId = response.data.id;

    }).catch(error => {
      //if error catched during process, reset view
      self.reset();
    });
  }

  //copy button to clipboard function
  set_link_button = (element, zipId) => {
    const el = document.createElement('textarea');
    el.value = window.origin + '/' + zipId;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    element.innerHTML = "Text copied";
  }
}

class DragAndDropComponent{
  dragging = 0;
  files = [];
  uploadFilesComponent = null;

  //require a upload files component to work
  constructor(uploadFilesComponent){
    this.uploadFilesComponent = uploadFilesComponent;
    const self = this;

    // drag & drop functions
    // for each dragged over cont, increment dragging
    // for each dragged over cont leaved, decrement dragging
    // if dragging > 0 show dragging background
    // if draggin <= 0 hide dragging background
    $(".drap_and_drop_container").addEventListener("drag", function(e){
      e.preventDefault();
      e.stopPropagation();
    });
    $(".drap_and_drop_container").addEventListener("dragstart", function(e){
      e.preventDefault();
      e.stopPropagation();
    });
    $(".drap_and_drop_container").addEventListener("dragend", function(e){
      e.preventDefault();
      e.stopPropagation();
      self.dragging--;
      if (self.dragging === 0) {
        $("#dragCont").style.display = "none";
      }
    });
    $(".drap_and_drop_container").addEventListener("dragover", function(e){
      e.preventDefault();
      e.stopPropagation();
      $("#dragCont").style.display = "flex";
    });
    $(".drap_and_drop_container").addEventListener("dragenter", function(e){
      e.preventDefault();
      e.stopPropagation();
      self.dragging++;
      $("#dragCont").style.display = "flex";
    });
    $(".drap_and_drop_container").addEventListener("dragleave", function(e){
      e.preventDefault();
      e.stopPropagation();
      self.dragging--;
      if (self.dragging === 0) {
        $("#dragCont").style.display = "none";
      }
    });
    $(".drap_and_drop_container").addEventListener("drop", function(e){
      e.preventDefault();
      e.stopPropagation();

      //reset dragging
      self.dragging = 0;
      if (self.dragging === 0) {
        $("#dragCont").style.display = "none";
      }

      //transfer event to UploadFilesComponent & act like a normal click on "add file" button
      self.uploadFilesComponent.files_importer(e.dataTransfer.files, self.uploadFilesComponent);
    });
  }
}

window.addEventListener("load", () => {
  let uploadFilesComponent = new UploadFilesComponent();
  new DragAndDropComponent(uploadFilesComponent);
});