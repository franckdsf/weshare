<div class="upload_files">
  <div id="add_files">
    <input type="file" id="files_importer" style="display:none;" multiple>
    <i class="fas fa-plus"></i>
    Add a file
  </div>
  <div id="files_cont">
  </div>
  <div class="message">
    <textarea placeholder="Message" id="message"></textarea>
  </div>
  <div id="button_link" class="button_link inactive">
    Generate the link
  </div>
  <div id="copy_button_link" class="button_link inactive">
    Copy the link
  </div>
</div>

<link rel="stylesheet" href="front/components/uploadFiles/uploadFiles.css" type ="text/css">
<script type="module" src="front/components/uploadFiles/uploadFiles.js"></script>
