<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>WeShare</title>
    <meta name="description" content="a sharing files plateform">
    <meta name="author" content="Lois chabrier, Franck Desfrancais">
    <link rel="icon" type="image/png" href="/front/assets/img/logo.png" />

    <meta property="og:title" content="We share, a sharing files platform" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Share your files with friends and transfer your data across your devices !" />
    <meta property="og:image" content="front/assets/img/og.jpg" />

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;1,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>

  <body>
    <?php require_once __DIR__.'/views/home/home.php'; ?>
  </body>
</html>