<div class="page drap_and_drop_container" id="home_page" style="display:none">
  <div class="header">
  <a href="/"><div class="logo"><img src="front/assets/img/logo.png"></div></a>
    <div class="container">
    </div>
  </div>
  <div class="container" id="background_parent">
    <div class="background_component"><?php require __DIR__.'/../../components/background/background.php' ?></div>
    <div class="upload_files_component" id="upload_files_component_1"><?php require __DIR__.'/../../components/uploadFiles/uploadFiles.php' ?></div>
    <div class="download_files_component" id="download_files_component_1"><?php require __DIR__.'/../../components/downloadFiles/downloadFiles.php' ?></div>
    <div class="title_container">
      <div class="title">WE <b>SHARE</b></div>
      <div class="subtitle">Upload your files.</div>
    </div>
  </div>
  <div id="dragCont" style="display:none">
    <span>Drop your files here.</span>
  </div>
  <div class="copyright">Franck Desfrançais, Loïs Chabrier © copyright 2020 - 2052</div>
</div>

<link rel="stylesheet" href="front/views/home/home.css" type ="text/css">
<script type="module" src="front/views/home/home.js"></script>