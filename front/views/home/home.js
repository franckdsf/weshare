import {$} from '../../assets/js/jquery_like.js';

class HomeComponent{
  constructor(){
    this.showBox();

    const self = this;
    $(".drap_and_drop_container").addEventListener("drop", function(e){
      e.preventDefault();
      self.setUploadView();
    });
  }

  setUploadView(){
    $("#upload_files_component_1").style.display = "block";
    $("#download_files_component_1").style.display = "none";
    window.history.pushState('WeShare', 'WeShare', window.location.origin);
    $("#home_page .title_container .subtitle").innerHTML = "Upload your files.";
    $(".header .container").style.display = "none";
  }

  showBox(){
    //if no params passed in URL
    //show upload files block
    if(window.location.pathname == "/"){
      $("#download_files_component_1").style.display = "none";
      $("#upload_files_component_1").style.display = "block";
    //else show download block
    }else{
      $("#upload_files_component_1").style.display = "none";
      $("#download_files_component_1").style.display = "block";

      //add a nav button to go back to upload files page
      const nav = document.createElement("nav");
      nav.innerHTML = "<nav>Upload my files</nav>";
      nav.addEventListener("click", ()=>{this.setUploadView();});
      $(".header .container").append(nav);
      $("#home_page .title_container .subtitle").innerHTML = "Retrieve your files.";
    }
  }
}
  
window.addEventListener("load", () => {
  $("#home_page").style.display = "block";
  new HomeComponent();
});